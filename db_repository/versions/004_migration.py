from sqlalchemy import *
from migrate import *


from migrate.changeset import schema
pre_meta = MetaData()
post_meta = MetaData()
allow_users = Table('allow_users', post_meta,
    Column('id', Integer, primary_key=True, nullable=False),
    Column('username', String(length=64)),
    Column('ipaddress', String(length=32)),
)

deny_users = Table('deny_users', post_meta,
    Column('id', Integer, primary_key=True, nullable=False),
    Column('username', String(length=64)),
    Column('ipaddress', String(length=32)),
)

ip_adress = Table('ip_adress', post_meta,
    Column('id', Integer, primary_key=True, nullable=False),
    Column('ipaddress', String(length=32)),
)

server = Table('server', post_meta,
    Column('id', Integer, primary_key=True, nullable=False),
    Column('ipaddress', String(length=64)),
    Column('hostname', String(length=64)),
)

user = Table('user', post_meta,
    Column('id', Integer, primary_key=True, nullable=False),
    Column('username', String(length=64)),
    Column('email', String(length=120)),
    Column('fullname', String(length=120)),
)


def upgrade(migrate_engine):
    # Upgrade operations go here. Don't create your own engine; bind
    # migrate_engine to your metadata
    pre_meta.bind = migrate_engine
    post_meta.bind = migrate_engine
    post_meta.tables['allow_users'].create()
    post_meta.tables['deny_users'].create()
    post_meta.tables['ip_adress'].create()
    post_meta.tables['server'].create()
    post_meta.tables['user'].columns['fullname'].create()


def downgrade(migrate_engine):
    # Operations to reverse the above upgrade go here.
    pre_meta.bind = migrate_engine
    post_meta.bind = migrate_engine
    post_meta.tables['allow_users'].drop()
    post_meta.tables['deny_users'].drop()
    post_meta.tables['ip_adress'].drop()
    post_meta.tables['server'].drop()
    post_meta.tables['user'].columns['fullname'].drop()
