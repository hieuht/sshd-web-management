#!/usr/bin/env python
#coding:utf-8
"""
  Author:  HieuHT --<hieuhatrung@admicro.vn>
  Purpose: Data Models
  Created: 08/28/2015
"""

########################################################################
from app import db

class Group(db.Model):
    id = db.Column(db.Integer, primary_key = True)
    name = db.Column(db.String(64), index = True, unique = True)

    def __repr__(self):
        return '<Group %r>' % (self.name)

class User(db.Model):
    id = db.Column(db.Integer, primary_key = True)
    username = db.Column(db.String(64), index = True, unique = True)
    email = db.Column(db.String(120), index = True, unique = True)
    fullname = db.Column(db.String(120), index = False, unique = False)

    def __repr__(self):
        return '<User %r>' % (self.username)
    
class Server(db.Model):
    id = db.Column(db.Integer, primary_key = True)
    ipaddress = db.Column(db.String(64), index = True, unique = True)
    hostname = db.Column(db.String(64), index = True, unique = True)
    
    def __repr__(self):
        return '<Server %r>' % (self.hostname)
    
class IPAdress(db.Model):
    id = db.Column(db.Integer, primary_key = True)    
    ipaddress = db.Column(db.String(32), index = True, unique = True)

    def __repr__(self):
        return '<IPAdress %r>' % (self.ipaddress)

class AllowUsers(db.Model):
    id = db.Column(db.Integer, primary_key = True)
    username = db.Column(db.String(64), index = True)
    ipaddress = db.Column(db.String(32), index = True)
    
    def __repr__(self):
        return '<AllowUsers %r>' % (self.id)
    

class DenyUsers(db.Model):
    id = db.Column(db.Integer, primary_key = True)
    username = db.Column(db.String(64), index = True)
    ipaddress = db.Column(db.String(32), index = True)
    
    def __repr__(self):
        return '<DenyUsers %r>' % (self.id)
    
