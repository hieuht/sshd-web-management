#!/usr/bin/env python
#coding:utf-8
"""
  Author:  HieuHT --<hieuhatrung@admicro.vn>
  Purpose: Data View Controllers
  Created: 08/28/2015
"""
from flask import render_template
from app import app, models, db

@app.route('/')
@app.route('/index', methods = ['GET'])
def index():
    data = {}    
    return render_template('index.html', data = data)

@app.route('/about', methods = ['GET'])
def about():
    data = {'active_about': True,}        
    return render_template('about.html', data = data)

@app.route('/contact', methods = ['GET'])
def contact():
    data = {'active_contact': True,}        
    return render_template('contact.html', data = data)

@app.route('/servers', methods = ['GET'])
def servers():
    data = {'active_servers': True,
            'title': 'Servers Management',}    
            
    return render_template('servers.html', data = data)

@app.route('/groups', methods = ['GET'])
def groups():
    data = {'active_groups': True,
            'title': 'Groups Management',}    
    return render_template('groups.html', data = data)
    
@app.route('/users', methods = ['GET'])
def users():
    data = {'active_users': True,
            'title': 'Users Management',}    
            
    return render_template('users.html', data = data)

@app.route('/ipaddress', methods = ['GET'])
def ipaddress():
    data = {'active_ipaddress': True,
            'title': 'IP Address Management',}    
    
    return render_template('ipaddress.html', data = data)

